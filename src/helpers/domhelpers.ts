import { IMovie } from './moviehelpers'
import { heartClickHandler } from '../eventListeners/heartClickHandler';

//creating element with class and attributes
export function createElement(tagName: string, className?: string, attributes?: { [key: string]: string }): HTMLElement {
    const element = document.createElement(tagName);

    if (className) {
        const classNames = className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    if (attributes) {
        Object.keys(attributes).forEach((key: string) =>
            element.setAttribute(key, attributes[key])
        )
    }
    return element;
}

//get heart svg element
export function getHeart(movie: IMovie): SVGSVGElement {
    const heart: SVGSVGElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    heart.setAttribute("stroke", "red");
    if (movie.id && localStorage.getItem(movie.id.toString())) {
        heart.setAttribute("fill", "red");
    }
    else {
        heart.setAttribute("fill", "#ff000078");
    }
    heart.setAttribute("width", "50");
    heart.setAttribute("height", "50");
    heart.setAttribute("xmlns", "http://www.w3.org/2000/svg");
    heart.setAttribute("class", "bi bi-heart-fill position-absolute p-2");
    heart.setAttribute("viewBox", "0 -2 18 22");
    const heartInside: SVGPathElement = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    heartInside.setAttributeNS(null, "fill-rule", "evenodd");
    heartInside.setAttributeNS(null, "d", "M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z");
    heart.append(heartInside);

    return heart;
}

//get full html-element of a card-movie
export function getCardBlock(movie: IMovie): HTMLElement {
    const imageURL = "https://image.tmdb.org/t/p/original";
    const div: HTMLElement = createElement("div", "col-lg-3 col-md-4 col-12 p-2");
    const divCard: HTMLElement = createElement("div", "card shadow-sm");
    const img: HTMLElement = createElement("img", "", {
        src: imageURL + movie.poster_path,
        alt: "No image here"
    });
    const cardBody = createElement("div", "card-body")
    const description = createElement("p", "card-text truncate")
    if (movie.overview) description.innerText = movie.overview;
    const dateDiv = createElement("div", "d-flex justify-content-between align-items-center");
    const dateP = createElement("small", "text-muted");
    const heart = getHeart(movie);
    heart.setAttribute('data-filmid', <string>movie.id?.toString());
    heart.addEventListener('click', () => {
        heartClickHandler(heart, movie);
    }, false);
    if (movie.release_date) {
        dateP.innerText = movie.release_date;
    }
    dateDiv.append(dateP);
    cardBody.append(description, dateDiv);
    divCard.append(img, heart, cardBody);
    div.append(divCard);
    return div;
}


//render every cardBlock and push it into container
export function renderVideoblockHTML(movies: IMovie[], toClear?: string): void {
    const container = document.getElementById("film-container");
    if (!container) throw Error("Didn't find container, press F5 and try again.");
    if (toClear == 'clear') {
        container.innerHTML = "";
    }
    movies.forEach((item: IMovie) => {
        const cardDiv = getCardBlock(item);
        container.append(cardDiv);
    })
}


//render block with favourite movies according to local-storage
export function renderFavouriteBlock(): void {
    const favouriteBlock = document.getElementById("favorite-movies");
    if (!favouriteBlock) throw Error("Press F5 please");
    favouriteBlock.innerHTML = "";
    const keys = Object.keys(localStorage);
    if (keys) {
        for (const key of keys) {
            const cardInfo = JSON.parse(<string>localStorage.getItem(key.toString()));
            const movie: IMovie = {
                id: cardInfo.id,
                poster_path: cardInfo.poster_path,
                original_title: cardInfo.original_title,
                overview: cardInfo.overview,
                release_date: cardInfo.release_date
            };
            const cardDiv = getCardBlock(movie);
            cardDiv.className = "col-12 p-2";
            favouriteBlock.append(cardDiv);
        }
    }
}

//when we delete a movie from favourite this changes heart color in container.
export function changeHeartColor(id: number): void {
    const container = document.getElementById("film-container");
    if (!container) throw Error("Didn't find container, press F5 and try again.");
    const hearts = container.querySelectorAll(".bi-heart-fill")
    for (let i = 0; i < hearts.length; i++) {
        if ((<SVGSVGElement>hearts[i]).dataset.filmid && (<SVGSVGElement>hearts[i]).dataset.filmid == id.toString()) {
            <unknown>hearts[i].setAttribute("fill", "#ff000078");
        }
    }
}