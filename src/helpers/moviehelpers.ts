import { accessKey, getMovieArr } from '../index'
//Interface for movie
export interface IMovie {
    readonly id: number,
    readonly poster_path: string | null,
    readonly overview: string | null,
    readonly release_date: string | null,
    readonly original_title: string | null
}

//does fetch and returns responce.json
export async function doFetch(baseURL: string, endpoint: string): Promise<{ results: Array<{ id: number, poster_path: string | null; overview: string | null; release_date: string | null; original_title: string | null }> }> {
    const responce = await fetch(baseURL + endpoint)
    const result = responce.json();
    return new Promise(resolve => resolve(result));
}

//gets only needed information
export function mapper(movie: { id: number, poster_path: string | null; overview: string | null; release_date: string | null; original_title: string | null }): IMovie {
    const result: IMovie = {
        id: movie.id,
        poster_path: movie.poster_path,
        overview: movie.overview,
        release_date: movie.release_date,
        original_title: movie.original_title
    };
    return result;
}

//setting a random video from the 1 page to the top
export async function setRandomVideo(): Promise<boolean> {
    const baseURL = "https://api.themoviedb.org/3/movie/popular?api_key=" + accessKey;
    const movies = await getMovieArr(baseURL);
    const numberOfMovie = getRandomInt(movies.length);
    const movie = movies[numberOfMovie]; // random movie
    const nameBlock: HTMLElement | null = document.getElementById("random-movie-name");
    const desctriptionBlock: HTMLElement | null = document.getElementById("random-movie-description");
    const imageBlock: HTMLElement | null = document.getElementById("random-movie");

    if (nameBlock && nameBlock.innerText && movie.original_title) {
        nameBlock.innerText = movie.original_title;
    }
    if (desctriptionBlock && desctriptionBlock.innerText && movie.overview) {
        desctriptionBlock.innerText = movie.overview;
    }
    if (imageBlock && movie.poster_path) {
        imageBlock.setAttribute(`style`,
            `background-image: url(https://image.tmdb.org/t/p/original${movie.poster_path});
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        `)
    }
    return new Promise(resolve => resolve(true));
}
//random int from 0 to max
function getRandomInt(max: number) {
    return Math.floor(Math.random() * max);
}