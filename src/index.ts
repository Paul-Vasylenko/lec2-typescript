import { renderVideoblockHTML } from "./helpers/domhelpers"
import { addAllListeners } from './eventListeners/listeners'
import { IMovie, doFetch, mapper } from './helpers/moviehelpers'
export const accessKey = "3240ada14876e613c1acf8d35d5063fc";
//onload we do popular - fetch
export let option: renderVariables = 'popular';
//onload we do fetch on page=1
export let pageNum = 1;

//main function
export async function render(): Promise<void> {
    addAllListeners();
    return;
}


//types of rendering the video-block
export type renderVariables = 'upcoming' | 'popular' | 'top rated' | 'search'

//gets baseURL and renders video-block
export async function renderVideoBlock(switcher: renderVariables, toClear?: string): Promise<boolean> {
    const baseURL = getBaseURL(switcher); // base page is equal to 1
    option = switcher;
    if (!toClear) pageNum++;
    const movies = await getMovieArr(baseURL);
    renderVideoblockHTML(movies, toClear)
    return new Promise(resolve => resolve(true));
}

//get URL according to btn clicked
function getBaseURL(switcher: renderVariables): string {
    let baseURL = "";
    if (switcher == 'upcoming') {
        baseURL = "https://api.themoviedb.org/3/movie/upcoming?api_key=" + accessKey;
    }
    else if (switcher == 'popular') {
        baseURL = "https://api.themoviedb.org/3/movie/popular?api_key=" + accessKey;
    }
    else if (switcher == 'top rated') {
        baseURL = "https://api.themoviedb.org/3/movie/top_rated?api_key=" + accessKey;
    }
    else if (switcher == 'search') {
        const searchValue = (<HTMLInputElement>document.getElementById("search"))?.value;
        baseURL = "https://api.themoviedb.org/3/search/movie?api_key=" + accessKey + "&query=" + searchValue; // add words to search by
    }
    return baseURL;
}

//after fetch gets an array of IMovie-objects
export async function getMovieArr(baseURL: string): Promise<IMovie[]> {
    let endpoint = "";
    if (pageNum != 1) {
        endpoint = "&page=" + pageNum;
    }
    const results = await doFetch(baseURL, endpoint);
    const fetchResult = results.results;
    const movies: IMovie[] = fetchResult.map((item: { id: number, poster_path: string | null; overview: string | null; release_date: string | null; }) => {
        return mapper(item);
    });;
    return new Promise((resolve) => resolve(movies));
}



