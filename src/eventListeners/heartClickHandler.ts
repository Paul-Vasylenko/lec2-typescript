import { IMovie } from "../helpers/moviehelpers";
import { renderFavouriteBlock, changeHeartColor } from "../helpers/domhelpers";

export function heartClickHandler(target: SVGSVGElement, movie: IMovie): void {
    //if is not liked
    if (target.getAttribute("fill") == '#ff000078') {
        target.setAttribute("fill", 'red');
        if (movie.id)
            localStorage.setItem(movie.id.toString(), JSON.stringify(movie));
        renderFavouriteBlock();
    }
    //if is liked
    else {
        target.setAttribute("fill", '#ff000078');
        if (movie.id)
            localStorage.removeItem(movie.id.toString());
        renderFavouriteBlock();
        changeHeartColor(<number>movie.id);
    }
}