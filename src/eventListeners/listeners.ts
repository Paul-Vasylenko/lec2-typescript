import { renderVideoBlock, option } from '../index'
import { setRandomVideo } from '../helpers/moviehelpers'
import { renderFavouriteBlock } from '../helpers/domhelpers';

//all listeners
export function addAllListeners(): void {
    const upcomingBlock = document.getElementById("upcoming");
    const popularBlock = document.getElementById("popular");
    const topBlock = document.getElementById("top_rated");
    const seachBtn = document.getElementById("submit");
    const loadMoreBtn = document.getElementById('load-more');

    window.onload = () => {
        renderVideoBlock('popular', 'clear')
        setRandomVideo();
        renderFavouriteBlock();
    }
    if (!upcomingBlock) throw Error("press F5 please");
    upcomingBlock.addEventListener("click", () => {
        renderVideoBlock('upcoming', 'clear')
    });
    if (!popularBlock) throw Error("Press F5 please");
    popularBlock.addEventListener("click", () => {
        renderVideoBlock('popular', 'clear')
    });
    if (!topBlock) throw Error("Press F5 please");
    topBlock.addEventListener("click", () => {
        renderVideoBlock('top rated', 'clear')
    });
    if (!seachBtn) throw Error("Press F5 please");
    seachBtn.addEventListener('click', () => {
        renderVideoBlock('search', 'clear');
    })
    if (!loadMoreBtn) throw Error("Press F5 please");
    loadMoreBtn.addEventListener('click', () => {
        renderVideoBlock(option);
    })
}